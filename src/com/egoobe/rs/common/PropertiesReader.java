package com.egoobe.rs.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

	private static PropertiesReader instance;
	private static Properties prop;
	
	private PropertiesReader() {
		//load a properties file
		prop = new Properties();
		try {
			prop.load(getClass().getResourceAsStream("config.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized PropertiesReader getInstance() {
		if(instance == null) {
			instance = new PropertiesReader();
		}
		return instance;
	}

	public String getProperty(String key) {
		return prop.getProperty(key);
	}

}
