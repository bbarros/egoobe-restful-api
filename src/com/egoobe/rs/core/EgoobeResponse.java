package com.egoobe.rs.core;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class EgoobeResponse {
	
	public static Response build(Response.Status status, String message, List<DBObject> result) {
		BasicDBObject responseobj = new BasicDBObject();
		
		// Build Meta Object
		if(message == null) {
			message = status.getReasonPhrase();
		}
		responseobj.put("meta", EgoobeResponse.getMeta(status.getStatusCode(), message));
		
		// Build Result Object
		if(result == null) {
			result = new ArrayList<DBObject>();
		}
		responseobj.put("result", result);
		
		return Response.status(status).entity(responseobj.toString()).build();
	}

	private static DBObject getMeta(int code, String msg) {
		BasicDBObject metaobj = new BasicDBObject();
		metaobj.put("code", code);
		metaobj.put("message", msg);
		return metaobj;
	}

}
