package com.egoobe.rs.core;

import com.egoobe.mongo.Connector;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class EgoobeConsistency {

	public static final int USER_ENTITY = 1;
	public static final int COMMENT_ENTITY = 2;
	public static final int PRODUCT_ENTITY = 3;
	public static final int VENUE_ENTITY = 4;
	

	public static void deleteRelations(int entityType, String entityId) {	
		// Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBCollection comments_coll = db.getCollection("comments");
		DBCollection products_coll = db.getCollection("products");
		DBCollection venues_coll = db.getCollection("venues");
		
		BasicDBObject query = null;
		
		DBCursor usersCS = null;
		DBCursor commentsCS = null;
		DBCursor productsCS = null;
		DBCursor venuesCS = null;
		
		BasicDBList userList = null;
		BasicDBList commentList = null;
		BasicDBList productList = null;
		BasicDBList venueList = null;
		
		switch (entityType) {
			case EgoobeConsistency.COMMENT_ENTITY:
				// Update User documents
				query = new BasicDBObject("comments", entityId);
				usersCS = users_coll.find(query);
				userList = new BasicDBList();
				userList.addAll(usersCS.toArray());
				for (int i=0; i<userList.size(); i++) {
					DBObject user = (BasicDBObject) userList.get(i);
					commentList = (BasicDBList) user.get("comments");
					commentList.remove(entityId);
					user.put("comments", commentList);
					users_coll.save(user);
				}
	
				// Update Product documents
				query = new BasicDBObject("comments", entityId);
				productsCS = products_coll.find(query);
				productList = new BasicDBList();
				productList.addAll(productsCS.toArray());
				for (int i=0; i<productList.size(); i++) {
					DBObject product = (BasicDBObject) productList.get(i);
					commentList = (BasicDBList) product.get("comments");
					commentList.remove(entityId);
					product.put("comments", commentList);
					products_coll.save(product);
				}
	
				// Update Venue documents
				query = new BasicDBObject("comments", entityId);
				venuesCS = venues_coll.find(query);
				venueList = new BasicDBList();
				venueList.addAll(venuesCS.toArray());
				for (int i=0; i<venueList.size(); i++) {
					DBObject venue = (BasicDBObject) venueList.get(i);
					commentList = (BasicDBList) venue.get("comments");
					commentList.remove(entityId);
					venue.put("comments", commentList);
					venues_coll.save(venue);
				}
	
				break;
				
			case EgoobeConsistency.USER_ENTITY:
				// Update Comment documents
				query = new BasicDBObject("user", entityId);
				comments_coll.remove(query);
	
				// Update Product documents
				query = new BasicDBObject("users", entityId);
				productsCS = products_coll.find(query);
				productList = new BasicDBList();
				productList.addAll(productsCS.toArray());
				for (int i=0; i<productList.size(); i++) {
					DBObject product = (BasicDBObject) productList.get(i);
					userList = (BasicDBList) product.get("users");
					userList.remove(entityId);
					product.put("users", userList);
					products_coll.save(product);
				}
	
				// Update Venue documents
				query = new BasicDBObject("users", entityId);
				venuesCS = venues_coll.find(query);
				venueList = new BasicDBList();
				venueList.addAll(venuesCS.toArray());
				for (int i=0; i<venueList.size(); i++) {
					DBObject venue = (BasicDBObject) venueList.get(i);
					userList = (BasicDBList) venue.get("users");
					userList.remove(entityId);
					venue.put("users", userList);
					venues_coll.save(venue);
				}
	
				break;
				
			case EgoobeConsistency.PRODUCT_ENTITY:
				// Update User documents
				query = new BasicDBObject("products", entityId);
				usersCS = users_coll.find(query);
				userList = new BasicDBList();
				userList.addAll(usersCS.toArray());
				for (int i=0; i<userList.size(); i++) {
					DBObject user = (BasicDBObject) userList.get(i);
					productList = (BasicDBList) user.get("products");
					productList.remove(entityId);
					user.put("products", productList);
					users_coll.save(user);
				}
	
				// Update Comment documents
				query = new BasicDBObject("product", entityId);
				commentsCS = comments_coll.find(query);
				commentList = new BasicDBList();
				commentList.addAll(commentsCS.toArray());
				for (int i=0; i<commentList.size(); i++) {
					DBObject comment = (BasicDBObject) userList.get(i);
					comment.removeField("product");
					comments_coll.save(comment);
				}
	
				// Update Venue documents
				query = new BasicDBObject("products", entityId);
				venuesCS = venues_coll.find(query);
				venueList = new BasicDBList();
				venueList.addAll(venuesCS.toArray());
				for (int i=0; i<venueList.size(); i++) {
					DBObject venue = (BasicDBObject) venueList.get(i);
					productList = (BasicDBList) venue.get("products");
					productList.remove(entityId);
					venue.put("products", productList);
					venues_coll.save(venue);
				}
				
				break;
				
			case EgoobeConsistency.VENUE_ENTITY:
				// Update User documents
				query = new BasicDBObject("venues", entityId);
				usersCS = users_coll.find(query);
				userList = new BasicDBList();
				userList.addAll(usersCS.toArray());
				for (int i=0; i<userList.size(); i++) {
					DBObject user = (BasicDBObject) userList.get(i);
					venueList = (BasicDBList) user.get("venues");
					venueList.remove(entityId);
					user.put("venues", venueList);
					users_coll.save(user);
				}
	
				// Update Comment documents
				query = new BasicDBObject("venue", entityId);
				commentsCS = comments_coll.find(query);
				commentList = new BasicDBList();
				commentList.addAll(commentsCS.toArray());
				for (int i=0; i<commentList.size(); i++) {
					DBObject comment = (BasicDBObject) userList.get(i);
					comment.removeField("venue");
					comments_coll.save(comment);
				}
				
				// Update Product documents
				query = new BasicDBObject("venues", entityId);
				productsCS = products_coll.find(query);
				productList = new BasicDBList();
				productList.addAll(productsCS.toArray());
				for (int i=0; i<productList.size(); i++) {
					DBObject product = (BasicDBObject) productList.get(i);
					venueList = (BasicDBList) product.get("venues");
					venueList.remove(entityId);
					product.put("venues", venueList);
					products_coll.save(product);
				}
				
				break;

			default:
				break;
		}

	}
	
}
