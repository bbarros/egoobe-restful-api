package com.egoobe.rs.core.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.egoobe.rs.core.EgoobeResponse;

public class BadParameterException extends WebApplicationException{
	
	private static final long serialVersionUID = 776545390877181544L;

	public BadParameterException() {
		super(EgoobeResponse.build(Response.Status.BAD_REQUEST, 
				"Parameter value not acceptable", null));
	}
	
	public BadParameterException(String value) {
		super(EgoobeResponse.build(Response.Status.BAD_REQUEST, 
				"Invalid Parameter: "+value+"", null));
	}

}
