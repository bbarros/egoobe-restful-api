package com.egoobe.rs.core.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.egoobe.rs.core.EgoobeResponse;

public class ParameterNotFoundException extends WebApplicationException {

	private static final long serialVersionUID = 5571904502956671631L;

	public ParameterNotFoundException() {
		super(EgoobeResponse.build(Response.Status.BAD_REQUEST, 
				"Requested Parameter Not Found", null));
	}
	
	public ParameterNotFoundException(String name) {
		super(EgoobeResponse.build(Response.Status.BAD_REQUEST, 
				name+" is a Requested Paramenter", null));
	}

}
