package com.egoobe.rs.core.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.egoobe.rs.core.EgoobeResponse;

public class ResourceNotModifiedException extends WebApplicationException{
	
	private static final long serialVersionUID = 9030126692388128877L;

	public ResourceNotModifiedException() {
		super(EgoobeResponse.build(Response.Status.FORBIDDEN, 
				"The resource was not modified", null));
	}
}
