package com.egoobe.rs.core.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.egoobe.rs.core.EgoobeResponse;

@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<java.lang.RuntimeException>{
	
	public Response toResponse(java.lang.RuntimeException e) {
		e.printStackTrace();
		return EgoobeResponse.build(Response.Status.INTERNAL_SERVER_ERROR, null, null);		
	}
}
