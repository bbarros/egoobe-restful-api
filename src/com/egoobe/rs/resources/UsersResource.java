package com.egoobe.rs.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import com.egoobe.mongo.Connector;
import com.egoobe.rs.common.PropertiesReader;
import com.egoobe.rs.core.EgoobeConsistency;
import com.egoobe.rs.core.EgoobeResponse;
import com.egoobe.rs.core.exceptions.BadParameterException;
import com.egoobe.rs.core.exceptions.ParameterNotFoundException;
import com.egoobe.utils.KeywordUtils;
import com.egoobe.utils.resource.CommentUtils;
import com.egoobe.utils.resource.ProductUtils;
import com.egoobe.utils.resource.UserUtils;
import com.egoobe.utils.resource.VenueUtils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Path("/users")
public class UsersResource {
		
	@GET
	@Produces("application/json")
	@Path("/auth/fs")
	public Response authenticateFoursquareUserbyQuery(
			@QueryParam("code") String code,
			@DefaultValue("false") @QueryParam("web") boolean web) {

		return this.authenticateFoursquareUser(code, web);
	}
	
	@GET
	@Produces("application/json")
	@Path("/auth/fs/{code}")
	public Response authenticateFoursquareUser(
			@PathParam("code") String code,
			@DefaultValue("false") @QueryParam("web") boolean web) {

		PropertiesReader pr = PropertiesReader.getInstance();
		String urlstr = null;
		
		if(web) { // Used to differentiate the redirect url
			urlstr ="https://foursquare.com/oauth2/access_token"+
					"?client_id="+ pr.getProperty("oauth.foursquare.web.clientid") +
				    "&client_secret="+ pr.getProperty("oauth.foursquare.web.clientsecret") +
				    "&grant_type=authorization_code"+
				    "&redirect_uri="+ pr.getProperty("oauth.foursquare.web.redirecturi") +
				    "&code="+ code;			
		} else {
			urlstr ="https://foursquare.com/oauth2/access_token"+
					"?client_id="+ pr.getProperty("oauth.foursquare.clientid") +
				    "&client_secret="+ pr.getProperty("oauth.foursquare.clientsecret") +
				    "&grant_type=authorization_code"+
				    "&redirect_uri="+ pr.getProperty("oauth.foursquare.redirecturi") +
				    "&code="+ code;			
		}
		
		String fstoken = new String();
		
		try {
			URL url = new URL(urlstr);

			URLConnection fsconn = url.openConnection();
			InputStream io_stream_server = fsconn.getInputStream();
			BufferedReader response = new BufferedReader(new InputStreamReader(io_stream_server, "UTF-8"));
			
			String response_json = response.readLine();
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory jf = new JsonFactory();
			JsonParser jp = jf.createJsonParser(response_json);
			JsonNode rootNode = mapper.readTree(jp);
			
			fstoken = rootNode.get("access_token").asText();

		} catch (MalformedURLException e) {
			throw new BadParameterException();
		} catch (IOException e) {
			throw new BadParameterException();
		}
		
		DBObject user = new BasicDBObject();
		BasicDBList result_list = new BasicDBList();
		
		String userUrlStr = "https://api.foursquare.com/v2/users/self?v=20121002&oauth_token="+ fstoken;
		try {
			URL userURL = new URL(userUrlStr);
			URLConnection fsconn = userURL.openConnection();
			InputStream io_stream_server = fsconn.getInputStream();
			BufferedReader response = new BufferedReader(new InputStreamReader(io_stream_server, "UTF-8"));
			
			String response_json = response.readLine();
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory jf = new JsonFactory();
			JsonParser jp = jf.createJsonParser(response_json);
			JsonNode rootNode = mapper.readTree(jp);
			JsonNode userNode = rootNode.get("response").get("user"); 

			//Connecting to MongoDB using connector
			Connector mongoconn = Connector.getInstance();
			DB db = mongoconn.getDb("egoobe");
			
			DBCollection users_coll = db.getCollection("users");
			
			// Verify replicated user
			BasicDBObject replicated_query = new BasicDBObject("authentication.foursquare.id", userNode.get("id").asText());
			DBObject userReplicated = users_coll.findOne(replicated_query);
			if(userReplicated != null) {
				user = userReplicated;
			}
			
			// Creating User Document
			user.put("name", userNode.get("firstName").asText() + " " + userNode.get("lastName").asText());
			BasicDBObject foursquare_auth = new BasicDBObject();
			foursquare_auth.put("id", userNode.get("id").asText());
			foursquare_auth.put("token", fstoken);
			user.put("authentication", new BasicDBObject("foursquare", foursquare_auth));
			BasicDBObject thumb = new BasicDBObject();
			String photoPrefix = userNode.get("photo").get("prefix").asText();
			String photoSufix = userNode.get("photo").get("suffix").asText();
			thumb.put("36x36", photoPrefix + "36x36" + photoSufix);
			thumb.put("100x100", photoPrefix + "100x100" + photoSufix);
			thumb.put("300x300", photoPrefix + "300x300" + photoSufix);
			user.put("thumbnail", thumb);
			user.put("contact", new BasicDBObject("email", userNode.get("contact").get("email").asText()));
			user.put("_keywords", UserUtils.generateKeywords(user));
			
			users_coll.save(user); // Persist User
			DBObject userObj = users_coll.findOne(user); // Retrieve User
			result_list.add(userObj);
			
		} catch (MalformedURLException e) {
			throw new RuntimeException();
		} catch (IOException e) {
			throw new RuntimeException();
		}
			
		List<DBObject> result = UserUtils.formatResponse(result_list);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	

	@GET
	@Produces("application/json")
	@Path("/auth/fb")
	public Response authenticateFacebookUserbyQuery(
			@QueryParam("code") String code,
			@DefaultValue("false") @QueryParam("web") boolean web) {

		return this.authenticateFacebookUser(code, web);
	}

	@GET
	@Produces("application/json")
	@Path("/auth/fb/{code}")
	public Response authenticateFacebookUser(
			@PathParam("code") String code,
			@DefaultValue("false") @QueryParam("web") boolean web) {
		
		PropertiesReader pr = PropertiesReader.getInstance();
		String urlstr = null;
		
		if(web) { // Used to differentiate the redirect url
			urlstr ="https://graph.facebook.com/oauth/access_token" +
					"?client_id=" + pr.getProperty("oauth.facebook.web.clientid") +
					"&client_secret=" + pr.getProperty("oauth.facebook.web.clientsecret") +
					"&redirect_uri=" + pr.getProperty("oauth.facebook.web.redirecturi") +
					"&code=" + code;
		} else {
			urlstr ="https://graph.facebook.com/oauth/access_token" +
					"?client_id=" + pr.getProperty("oauth.facebook.clientid") +
					"&client_secret=" + pr.getProperty("oauth.facebook.clientsecret") +
					"&redirect_uri=" + pr.getProperty("oauth.facebook.redirecturi") +
					"&code=" + code;			
		}
		
		
		String fbtoken = new String();
		String fbexpire = new String();
		
		try {
			URL url = new URL(urlstr);

			URLConnection fbconn = url.openConnection();
			InputStream io_stream_server = fbconn.getInputStream();
			BufferedReader response = new BufferedReader(new InputStreamReader(io_stream_server, "UTF-8"));
			
			String response_line = response.readLine();
			String[] response_vector = response_line.split("&");
			String response_token = response_vector[0];
			String response_expire = response_vector[1];
			
			fbtoken = response_token.split("=")[1];
			fbexpire = response_expire.split("=")[1];

		} catch (MalformedURLException e) {
			throw new BadParameterException();
		} catch (IOException e) {
			throw new BadParameterException();
		}
				
		DBObject user = new BasicDBObject();
		BasicDBList result_list = new BasicDBList();
		
		String userUrlStr = "https://graph.facebook.com/me?access_token="+ fbtoken;
		try {
			URL userURL = new URL(userUrlStr);
			URLConnection fbconn = userURL.openConnection();
			InputStream io_stream_server = fbconn.getInputStream();
			BufferedReader response = new BufferedReader(new InputStreamReader(io_stream_server, "UTF-8"));
			
			String response_json = response.readLine();
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory jf = new JsonFactory();
			JsonParser jp = jf.createJsonParser(response_json);
			JsonNode userNode = mapper.readTree(jp);

			//Connecting to MongoDB using connector
			Connector mongoconn = Connector.getInstance();
			DB db = mongoconn.getDb("egoobe");
			
			DBCollection users_coll = db.getCollection("users");

			// Verify replicated user
			BasicDBObject replicated_query = new BasicDBObject("authentication.facebook.id", userNode.get("id").asText());
			DBObject userReplicated = users_coll.findOne(replicated_query);
			if(userReplicated != null) {
				user = userReplicated;
			}

			// Creating User Document
			user.put("name", userNode.get("name").asText());
			BasicDBObject facebook_auth = new BasicDBObject();
			facebook_auth.put("id", userNode.get("id").asText());
			facebook_auth.put("token", fbtoken);
			facebook_auth.put("expire", fbexpire);
			user.put("authentication", new BasicDBObject("facebook", facebook_auth));
			BasicDBObject thumb = new BasicDBObject();
			String photoPrefix = "http://graph.facebook.com/" + userNode.get("username").asText() + "/picture";
			thumb.put("36x36", photoPrefix + "?width=36&height=36");
			thumb.put("100x100", photoPrefix + "?width=100&height=100");
			thumb.put("300x300", photoPrefix + "?width=300&height=300");
			user.put("thumbnail", thumb);
			user.put("contact", new BasicDBObject("email", userNode.get("email").asText()));
			user.put("_keywords", UserUtils.generateKeywords(user));
						
			users_coll.save(user); // Persist User
			DBObject userObj = users_coll.findOne(user); // Retrieve User
			result_list.add(userObj);
			
		} catch (MalformedURLException e) {
			throw new RuntimeException();
		} catch (IOException e) {
			throw new RuntimeException();
		}
			
		List<DBObject> result = UserUtils.formatResponse(result_list);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	public Response getUsers(
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBCursor cs = users_coll.find().skip((page-1)*result_limit).limit(result_limit);
		
		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		List<DBObject> result = UserUtils.formatResponse(csList);
				
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}")
	public Response getUser(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject query_id = new BasicDBObject("_id", new ObjectId(id));
		DBObject user = users_coll.findOne(query_id);
		
		BasicDBList csList = new BasicDBList();
		csList.add(user);
		List<DBObject> result = UserUtils.formatResponse(csList);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}


	@GET
	@Produces("application/json")
	@Path("/search")
	public Response searchUser(
			@DefaultValue("") @QueryParam("query") String query,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		if(query.compareTo("") == 0) {
			throw new ParameterNotFoundException();
		}
		/* =================================================================================*/

		
		BasicDBObject query_search = new BasicDBObject();
		
		// Handling query parameter
		query_search.put("_keywords", new BasicDBObject("$in", KeywordUtils.getKeywords(query)));
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		// Creating aggregation command
		BasicDBObject cmd = new BasicDBObject("aggregate", "users");
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
		
		BasicDBObject pipeline_match = new BasicDBObject("$match", query_search);
		pipeline.add(pipeline_match); // add match keywords
		
		BasicDBObject pipeline_unwind = new BasicDBObject("$unwind", "$_keywords");
		pipeline.add(pipeline_unwind); // add unwind over _keywords fields
		
		pipeline.add(pipeline_match); // add match over unwinded keywords
		
		BasicDBObject pipeline_group_fields = new BasicDBObject();
		pipeline_group_fields.put("_id", "$_id");
		pipeline_group_fields.put("name", new BasicDBObject("$first", "$name"));
		pipeline_group_fields.put("thumbnail", new BasicDBObject("$first", "$thumbnail"));
		pipeline_group_fields.put("contact", new BasicDBObject("$first", "$contact"));
		pipeline_group_fields.put("relevance", new BasicDBObject("$sum", 1));
		BasicDBObject pipeline_group = new BasicDBObject("$group", pipeline_group_fields);
		pipeline.add(pipeline_group); // add group
		
		BasicDBObject pipeline_sort = new BasicDBObject("$sort", new BasicDBObject("relevance", -1));
		pipeline.add(pipeline_sort); // add sort
		
		BasicDBObject pipeline_skip = new BasicDBObject("$skip", (page-1)*result_limit);
		pipeline.add(pipeline_skip); // add skip
		
		BasicDBObject pipeline_limit = new BasicDBObject("$limit", result_limit);
		pipeline.add(pipeline_limit); // add limit
		
		cmd.put("pipeline", pipeline);
		CommandResult cmd_result = db.command(cmd);
		
		List<DBObject> result = UserUtils.formatResponse((BasicDBList)cmd_result.get("result"));
				
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@DELETE
	@Produces("application/json")
	@Path("/{id}")
	public Response deleteUser(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		// Removing entity relations
		EgoobeConsistency.deleteRelations(EgoobeConsistency.USER_ENTITY, id);
		
		DBCollection users_coll = db.getCollection("users");
		users_coll.remove(new BasicDBObject("_id", new ObjectId(id)));

		return EgoobeResponse.build(Response.Status.NO_CONTENT, null, null);
	}
	
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/addproduct")
	public Response addProduct(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("productId") String productId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(productId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the product was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update User document
		BasicDBList productList = (BasicDBList) user.get("products");
		if(productList == null) { // verify if product list wasn't initialized
			productList = new BasicDBList();
		}
		
		if(!productList.contains(productId)) { // verify replication
			productList.add(productId);
			user.put("products", productList);
			users_coll.save(user);
		}
				
		// Update Product document
		BasicDBList userList = (BasicDBList) product.get("users");
		if(userList == null) { // verify if users list wasn't initialized
			userList = new BasicDBList();
		}
		
		if(!userList.contains(id)) { // verify replication
			userList.add(id);
			product.put("users", userList);
			products_coll.save(product);
		}
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}

	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/delproduct")
	public Response deleteProduct(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("productId") String productId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(productId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the product was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update User document
		BasicDBList productList = (BasicDBList) user.get("products");
		if(productList != null) { // verify if product list wasn't initialized
			if(productList.contains(productId)) { // verify replication
				productList.remove(productId);
				user.put("products", productList);
				users_coll.save(user);
			}
		}
				
		// Update Product document
		BasicDBList userList = (BasicDBList) product.get("users");
		if(userList != null) { // verify if user list wasn't initialized
			if(userList.contains(id)) { // verify replication
				userList.remove(id);
				product.put("users", userList);
				products_coll.save(product);
			}
		}
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}/products")
	public Response getProducts(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection products_coll = db.getCollection("products");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList productList = (BasicDBList) user.get("products");
		if(productList == null) { // verify if product list wasn't initialized
			productList = new BasicDBList();
		}

		List<ObjectId> productListOID = new ArrayList<ObjectId>();
		for (Object product : productList) { // Convert product list to ObjectId list
			productListOID.add(new ObjectId(product.toString()));
		}
		DBObject query_products = new BasicDBObject("_id", new BasicDBObject("$in", productListOID));
		DBCursor cs = products_coll.find(query_products).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = ProductUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/addvenue")
	public Response addVenue(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("venueId") String venueId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(venueId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update User document
		BasicDBList venueList = (BasicDBList) user.get("venues");
		if(venueList == null) { // verify if venues list wasn't initialized
			venueList = new BasicDBList();
		}
		
		if(!venueList.contains(venueId)) { // verify replication
			venueList.add(venueId);
			user.put("venues", venueList);
			users_coll.save(user);
		}
				
		// Update Venue document
		BasicDBList userList = (BasicDBList) venue.get("users");
		if(userList == null) { // verify if users list wasn't initialized
			userList = new BasicDBList();
		}
		
		if(!userList.contains(id)) { // verify replication
			userList.add(id);
			venue.put("users", userList);
			venues_coll.save(venue);
		}
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}


	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/delvenue")
	public Response deleteVenue(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("venueId") String venueId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(venueId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update User document
		BasicDBList venueList = (BasicDBList) user.get("venues");
		if(venueList != null) { // verify if venue list wasn't initialized
			if(venueList.contains(venueId)) { // verify replication
				venueList.remove(venueId);
				user.put("venues", venueList);
				users_coll.save(user);
			}
		}
				
		// Update Venue document
		BasicDBList userList = (BasicDBList) venue.get("users");
		if(userList != null) { // verify if user list wasn't initialized
			if(userList.contains(id)) { // verify replication
				userList.remove(id);
				venue.put("users", userList);
				venues_coll.save(venue);
			}
		}
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/{id}/venues")
	public Response getVenues(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection venues_coll = db.getCollection("venues");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList venueList = (BasicDBList) user.get("venues");
		if(venueList == null) { // verify if venues list wasn't initialized
			venueList = new BasicDBList();
		}

		List<ObjectId> venueListOID = new ArrayList<ObjectId>();
		for (Object venue : venueList) { // Convert venue list to ObjectId list
			venueListOID.add(new ObjectId(venue.toString()));
		}
		DBObject query_venues = new BasicDBObject("_id", new BasicDBObject("$in", venueListOID));
		DBCursor cs = venues_coll.find(query_venues).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = VenueUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/{id}/comments")
	public Response getComments(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection comments_coll = db.getCollection("comments");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList commentsList = (BasicDBList) user.get("comments");
		if(commentsList == null) { // verify if comments list wasn't initialized
			commentsList = new BasicDBList();
		}

		List<ObjectId> commentsListOID = new ArrayList<ObjectId>();
		for (Object comment : commentsList) { // Convert comments list to ObjectId list
			commentsListOID.add(new ObjectId(comment.toString()));
		}
		DBObject query_venues = new BasicDBObject("_id", new BasicDBObject("$in", commentsListOID));
		DBCursor cs = comments_coll.find(query_venues).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = CommentUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

}
