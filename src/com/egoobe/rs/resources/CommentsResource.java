package com.egoobe.rs.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import com.egoobe.mongo.Connector;
import com.egoobe.rs.core.EgoobeConsistency;
import com.egoobe.rs.core.EgoobeResponse;
import com.egoobe.rs.core.exceptions.BadParameterException;
import com.egoobe.utils.resource.CommentUtils;
import com.egoobe.utils.resource.ProductUtils;
import com.egoobe.utils.resource.UserUtils;
import com.egoobe.utils.resource.VenueUtils;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Path("/comments")
public class CommentsResource {

	@GET
	@Produces("application/json")
	public Response getComments(
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection comments_coll = db.getCollection("comments");
		DBCursor cs = comments_coll.find().skip((page-1)*result_limit).limit(result_limit);
		
		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		List<DBObject> result = CommentUtils.formatResponse(csList);
				
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}")
	public Response getComment(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection comments_coll = db.getCollection("comments");
		DBObject comment = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for comment id
			comment = comments_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(comment == null) { // check if the comment was found
			throw new BadParameterException();
		}
		/* =================================================================================*/
		
		BasicDBList csList = new BasicDBList();
		csList.add(comment);
		List<DBObject> result = CommentUtils.formatResponse(csList);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@PUT
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	public Response insertComment(
			@DefaultValue("") @FormParam("userId") String userId,
			@DefaultValue("") @FormParam("productId") String productId,
			@DefaultValue("") @FormParam("venueId") String venueId,
			@DefaultValue("") @FormParam("message") String message) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection comments_coll = db.getCollection("comments");
		DBObject comment = null;
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(userId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) { // check if the user was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(productId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the venue was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(venueId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}

		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();

		comment = new BasicDBObject();
		comment.put("user", userId);
		comment.put("venue", venueId);
		comment.put("product", productId);
		comment.put("message", message);
		Date now = new Date();
		comment.put("timestamp", now);
		comment.put("_keywords", CommentUtils.generateKeywords(comment));
		
		comments_coll.save(comment); // persist comment
		DBObject commentObj = comments_coll.findOne(comment); // retrieve comment
		
		BasicDBList commentList = null;		
		BasicDBList venueList = null;		
		BasicDBList productList = null;		

		// Update User document
		commentList = (BasicDBList) user.get("comments");
		if(commentList == null) { // verify if comment list wasn't initialized
			commentList = new BasicDBList();
		}
		commentList.add(commentObj.get("_id").toString());
		user.put("comments", commentList);
		users_coll.save(user);
		
		// Update Product document
		commentList = (BasicDBList) product.get("comments");
		if(commentList == null) { // verify if comment list wasn't initialized
			commentList = new BasicDBList();
		}
		commentList.add(commentObj.get("_id").toString());
		product.put("comments", commentList);
		venueList = (BasicDBList) product.get("venues");
		if(venueList == null) { // verify if venue list wasn't initialized
			venueList = new BasicDBList();
		}
		venueList.add(venue.get("_id").toString());
		product.put("venues", venueList);
		products_coll.save(product);

		// Update Venue document
		commentList = (BasicDBList) venue.get("comments");
		if(commentList == null) { // verify if comment list wasn't initialized
			commentList = new BasicDBList();
		}
		commentList.add(commentObj.get("_id").toString());
		venue.put("comments", commentList);
		productList = (BasicDBList) venue.get("products");
		if(productList == null) { // verify if product list wasn't initialized
			productList = new BasicDBList();
		}
		productList.add(product.get("_id").toString());
		venue.put("products", productList);
		venues_coll.save(venue);
		
		
		BasicDBList csList = new BasicDBList();
		csList.add(commentObj);
		result = CommentUtils.formatResponse(csList);			
		
		return EgoobeResponse.build(Response.Status.CREATED, null, result);
	}

	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}")
	public Response updateComment(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("message") String message) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection comments_coll = db.getCollection("comments");
		DBObject comment = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		if(message == "") {
			throw new BadParameterException();			
		}
		
		try { // verify consistency of ObjectId for comment id
			comment = comments_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(comment == null) { // check if the user was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();

		comment.put("message", message);
		Date now = new Date();
		comment.put("timestamp", now);
		comment.put("_keywords", CommentUtils.generateKeywords(comment));
		
		comments_coll.save(comment); // persist comment
		DBObject commentObj = comments_coll.findOne(comment); // retrieve comment
				
		BasicDBList csList = new BasicDBList();
		csList.add(commentObj);
		result = CommentUtils.formatResponse(csList);			
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@DELETE
	@Produces("application/json")
	@Path("/{id}")
	public Response deleteComment(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		// Removing entity relations
		EgoobeConsistency.deleteRelations(EgoobeConsistency.COMMENT_ENTITY, id);
		
		DBCollection comment_coll = db.getCollection("comments");

		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId
			comment_coll.remove(new BasicDBObject("_id", new ObjectId(id))); // remove comment
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException(id);
		}
		/* =================================================================================*/

		return EgoobeResponse.build(Response.Status.NO_CONTENT, null, null);
	}

	@GET
	@Produces("application/json")
	@Path("/{id}/user")
	public Response getUser(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection comments_coll = db.getCollection("comments");
		DBObject comment = null;
		DBCollection users_coll = db.getCollection("users");
		DBObject user = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for comment id
			comment = comments_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(comment == null) { // check if the comment was found
			throw new BadParameterException();
		}
		/* =================================================================================*/
		
		ObjectId userOID = new ObjectId(comment.get("user").toString());
		user = users_coll.findOne(new BasicDBObject("_id", userOID));
		
		BasicDBList csList = new BasicDBList();
		csList.add(user);
		List<DBObject> result = UserUtils.formatResponse(csList);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}/product")
	public Response getProduct(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection comments_coll = db.getCollection("comments");
		DBObject comment = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for comment id
			comment = comments_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(comment == null) { // check if the comment was found
			throw new BadParameterException();
		}
		/* =================================================================================*/
		
		ObjectId productOID = new ObjectId(comment.get("product").toString());
		product = products_coll.findOne(new BasicDBObject("_id", productOID));
		
		BasicDBList csList = new BasicDBList();
		csList.add(product);
		List<DBObject> result = ProductUtils.formatResponse(csList);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/{id}/venue")
	public Response getVenue(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection comments_coll = db.getCollection("comments");
		DBObject comment = null;
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for comment id
			comment = comments_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(comment == null) { // check if the comment was found
			throw new BadParameterException();
		}
		/* =================================================================================*/
		
		ObjectId venueOID = new ObjectId(comment.get("venue").toString());
		venue = venues_coll.findOne(new BasicDBObject("_id", venueOID));
		
		BasicDBList csList = new BasicDBList();
		csList.add(venue);
		List<DBObject> result = VenueUtils.formatResponse(csList);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

}
