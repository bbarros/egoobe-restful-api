package com.egoobe.rs.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import com.egoobe.mongo.Connector;
import com.egoobe.rs.core.EgoobeConsistency;
import com.egoobe.rs.core.EgoobeResponse;
import com.egoobe.rs.core.exceptions.BadParameterException;
import com.egoobe.rs.core.exceptions.ParameterNotFoundException;
import com.egoobe.utils.KeywordUtils;
import com.egoobe.utils.LatLngUtils;
import com.egoobe.utils.StringUtils;
import com.egoobe.utils.resource.CommentUtils;
import com.egoobe.utils.resource.ProductUtils;
import com.egoobe.utils.resource.UserUtils;
import com.egoobe.utils.resource.VenueUtils;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Path("/products")
public class ProductsResource {

	@GET
	@Produces("application/json")
	public Response getProducts(
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection products_coll = db.getCollection("products");
		DBCursor cs = products_coll.find().skip((page-1)*result_limit).limit(result_limit);
		
		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		List<DBObject> result = ProductUtils.formatResponse(csList);
				
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/{id}")
	public Response getProduct(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the product was found
			throw new BadParameterException();
		}
		/* =================================================================================*/
		
		BasicDBList csList = new BasicDBList();
		csList.add(product);
		List<DBObject> result = ProductUtils.formatResponse(csList);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	@Path("/search")
	public Response searchProduct(
			@DefaultValue("") @QueryParam("query") String query,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		if(query.compareTo("") == 0) {
			throw new ParameterNotFoundException();
		}
		/* =================================================================================*/

		
		BasicDBObject query_search = new BasicDBObject();
		
		// Handling query parameter
		query_search.put("_keywords", new BasicDBObject("$in", KeywordUtils.getKeywords(query)));
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		// Creating aggregation command
		BasicDBObject cmd = new BasicDBObject("aggregate", "products");
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
		
		BasicDBObject pipeline_match = new BasicDBObject("$match", query_search);
		pipeline.add(pipeline_match); // add match keywords
		
		BasicDBObject pipeline_unwind = new BasicDBObject("$unwind", "$_keywords");
		pipeline.add(pipeline_unwind); // add unwind over _keywords fields
		
		pipeline.add(pipeline_match); // add match over unwinded keywords
		
		BasicDBObject pipeline_group_fields = new BasicDBObject();
		pipeline_group_fields.put("_id", "$_id");
		pipeline_group_fields.put("name", new BasicDBObject("$first", "$name"));
		pipeline_group_fields.put("thumbnail", new BasicDBObject("$first", "$thumbnail"));
		pipeline_group_fields.put("tags", new BasicDBObject("$first", "$tags"));
		pipeline_group_fields.put("relevance", new BasicDBObject("$sum", 1));
		BasicDBObject pipeline_group = new BasicDBObject("$group", pipeline_group_fields);
		pipeline.add(pipeline_group); // add group
		
		BasicDBObject pipeline_sort = new BasicDBObject("$sort", new BasicDBObject("relevance", -1));
		pipeline.add(pipeline_sort); // add sort
		
		BasicDBObject pipeline_skip = new BasicDBObject("$skip", (page-1)*result_limit);
		pipeline.add(pipeline_skip); // add skip
		
		BasicDBObject pipeline_limit = new BasicDBObject("$limit", result_limit);
		pipeline.add(pipeline_limit); // add limit
		
		cmd.put("pipeline", pipeline);
		CommandResult cmd_result = db.command(cmd);
		
		List<DBObject> result = ProductUtils.formatResponse((BasicDBList)cmd_result.get("result"));
				
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	

	@GET
	@Produces("application/json")
	@Path("/search2")
	public Response searchProductText(
			@DefaultValue("") @QueryParam("query") String query,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		if(query.compareTo("") == 0) {
			throw new ParameterNotFoundException();
		}
		/* =================================================================================*/

		// Creating text search command
		BasicDBObject text_search = new BasicDBObject();		
		text_search.put("text", "products");
		text_search.put("search", query);
				
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		CommandResult cmd_result = db.command(text_search);
		
		List<DBObject> result = ProductUtils.formatResponseTextSearch((BasicDBList)cmd_result.get("results"));
				
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@PUT
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	public Response insertProduct(
			@DefaultValue("") @FormParam("userid") String userid,
			@DefaultValue("") @FormParam("name") String name,
			@DefaultValue("") @FormParam("thumbnail") String thumbnail,
			@DefaultValue("") @FormParam("tags") String tags) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");

		DBCollection users_coll = db.getCollection("users");		
		DBObject user = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		if(userid.compareTo("") == 0) {
			throw new ParameterNotFoundException();
		}
		
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(userid)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) {
			throw new ParameterNotFoundException();			
		}
		
		if(name.compareTo("") == 0) {
			throw new ParameterNotFoundException();
		}
		/* =================================================================================*/

		
		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBObject product = new BasicDBObject();
		product.put("name", name);
		/* TODO: product.put("thumbnail", new BasicDBObject("200x200", thumbnail))
		 * Before do that, we need to handle the image so we can get diffent thumbnail sizes.
		 * Thumbnail value must be changed for local thumbnail links*/
		List<String> tagList = StringUtils.parseCommaList(tags);
		product.put("tags", tagList);
		product.put("createdby", userid);

		product.put("_keywords", ProductUtils.generateKeywords(product));

		DBCollection products_coll = db.getCollection("products");
		products_coll.save(product); // persist product
		DBObject productObj = products_coll.findOne(product); // retrieve product
		
		BasicDBList csList = new BasicDBList();
		csList.add(productObj);
		result = ProductUtils.formatResponse(csList);			

		return EgoobeResponse.build(Response.Status.CREATED, null, result);
	}

	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}")
	public Response updateProduct(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("name") String name,
			@DefaultValue("") @FormParam("thumbnail") String thumbnail,
			@DefaultValue("") @FormParam("tags") String tags) {
		
		List<DBObject> result = new ArrayList<DBObject>();
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection products_coll = db.getCollection("products");
		DBObject product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));
		
		if(name.compareTo("") != 0) {
			product.put("name", name);			
		}

		if(thumbnail.compareTo("") != 0) {
			product.removeField("thumbnail");
			/* TODO: product.put("thumbnail", new BasicDBObject("200x200", thumbnail))
			 * Before do that, we need to handle the image so we can get diffent thumbnail sizes.
			 * Thumbnail value must be changed for local thumbnail links*/
		}
		
		if(tags.compareTo("") != 0) {
			product.removeField("tags");
			List<String> tagList = StringUtils.parseCommaList(tags);
			product.put("tags", tagList);			
		}

		product.put("_keywords", ProductUtils.generateKeywords(product));
		
		products_coll.save(product); // persist product
		DBObject productObj = products_coll.findOne(product); // retrieve product
		
		BasicDBList csList = new BasicDBList();
		csList.add(productObj);
		result = ProductUtils.formatResponse(csList);	
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@DELETE
	@Produces("application/json")
	@Path("/{id}")
	public Response deleteProduct(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		// Removing entity relations
		EgoobeConsistency.deleteRelations(EgoobeConsistency.PRODUCT_ENTITY, id);
		
		DBCollection products_coll = db.getCollection("products");
		products_coll.remove(new BasicDBObject("_id", new ObjectId(id)));

		return EgoobeResponse.build(Response.Status.NO_CONTENT, null, null);
	}


	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/addvenue")
	public Response addVenue(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("venueId") String venueId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the venue was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(venueId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update Product document
		BasicDBList venuesList = (BasicDBList) product.get("venues");
		if(venuesList == null) { // verify if venue list wasn't initialized
			venuesList = new BasicDBList();
		}
		
		if(!venuesList.contains(venueId)) { // verify replication
			venuesList.add(venueId);
			product.put("venues", venuesList);
			products_coll.save(product);
		}
		
		// Update Venue document
		BasicDBList productList = (BasicDBList) venue.get("products");
		if(productList == null) { // verify if product list wasn't initialized
			productList = new BasicDBList();
		}
		
		if(!productList.contains(id)) { // verify replication
			productList.add(id);
			venue.put("products", productList);
			venues_coll.save(venue);
		}
				
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}

	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/delvenue")
	public Response delVenue(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("venueId") String venueId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the venue was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(venueId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update Product document
		BasicDBList venuesList = (BasicDBList) product.get("venues");
		if(venuesList != null) { // verify if venue list wasn't initialized
			if(venuesList.contains(venueId)) { // verify replication
				venuesList.remove(venueId);
				product.put("venues", venuesList);
				products_coll.save(product);
			}
		}
		
		// Update Venue document
		BasicDBList productList = (BasicDBList) venue.get("products");
		if(productList != null) { // verify if product list wasn't initialized
			if(productList.contains(id)) { // verify replication
				productList.remove(id);
				venue.put("products", productList);
				venues_coll.save(venue);
			}
		}
				
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}

	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/cleanvenues")
	public Response cleanVenues(@PathParam("id") String id) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update Venue document
		BasicDBList venueList = (BasicDBList) product.get("venues");
		if(venueList == null) { // verify if product list wasn't initialized
			venueList = new BasicDBList();
		}
		
		for (Object venueId : venueList) {
			String vid = venueId.toString();
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(vid)));
			if(venue != null) {
				BasicDBList productList = (BasicDBList) venue.get("products");
				if(productList != null) { // verify if venue list wasn't initialized
					if(productList.contains(id)) { // verify replication
						productList.remove(id);
						venue.put("products", productList);
						venues_coll.save(venue);
					}
				}
			}
		}
		
		// Update Product document		
		product.removeField("venues");
		products_coll.save(product);
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/{id}/venues")
	public Response getVenues(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		DBCollection venues_coll = db.getCollection("venues");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList venueList = (BasicDBList) product.get("venues");
		if(venueList == null) { // verify if product list wasn't initialized
			venueList = new BasicDBList();
		}

		List<ObjectId> venueListOID = new ArrayList<ObjectId>();
		for (Object venue : venueList) { // Convert venue list to ObjectId list
			venueListOID.add(new ObjectId(venue.toString()));
		}
		DBObject query_venues = new BasicDBObject("_id", new BasicDBObject("$in", venueListOID));
		DBCursor cs = venues_coll.find(query_venues).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = VenueUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}/venuesnear")
	public Response getVenuesNear(
			@PathParam("id") String id,
			@DefaultValue("") @QueryParam("ll") String latlng,
			@DefaultValue("5000") @QueryParam("radius") String radius,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;

		/*
		 *  Parameters Validation Routine ==================================================
		 */
		List<Double> latlnglist = new ArrayList<Double>();
		if(latlng.compareTo("") != 0) { // verify required 'll' parameter
			try { // verify 'll' syntax
				String[] vlatlng = latlng.split(",");
				latlnglist.add(new Double(vlatlng[0])); // add latitude
				latlnglist.add(new Double(vlatlng[1])); // add longitude				
			} catch (Exception e) {
				throw new BadParameterException();
			}			
		} else {
			throw new ParameterNotFoundException();
		}
		
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the venue was found
			throw new BadParameterException();
		}

		/* =================================================================================*/
		
		// Getting all product locations
		BasicDBList venuesList = (BasicDBList) product.get("venues");
		if(venuesList == null) { // verify if venue list wasn't initialized
			venuesList = new BasicDBList();
		}
		List<ObjectId> venueListOID = new ArrayList<ObjectId>();
		for (Object venueId : venuesList) { // Convert venue list to ObjectId list
			venueListOID.add(new ObjectId(venueId.toString()));
		}
				
		BasicDBObject geonear = new BasicDBObject("geoNear", "venues");
		BasicDBObject geonear_query = new BasicDBObject();

		// Adding near parameter to geoNear command
		geonear.put("near", latlnglist); // adding $near operator to location query

		// Adding radius parameter
		if(radius.compareTo("") != 0) {
			Double max_radius = LatLngUtils.metersToDegrees(new Double(radius));
			geonear.put("maxDistance", max_radius); // adding $maxDistance operator to location query			
		}
		
		// Adding num parameter
		geonear.put("num", result_limit*page);
				
		// Adding query over venues list
		geonear_query.put("_id", new BasicDBObject("$in", venueListOID));
		
		// Adding query to GeoNear Command
		geonear.put("query", geonear_query);
		
		// Running Command
		CommandResult cmd_result = db.command(geonear);
		
		// Selecting result page
		BasicDBList cmd_result_list = (BasicDBList)cmd_result.get("results");
		BasicDBList cmd_result_page = new BasicDBList();
		for(int i=(page-1)*result_limit; i < cmd_result_list.size(); i++) {
			cmd_result_page.add(cmd_result_list.get(i));
		}
		
		List<DBObject> result = VenueUtils.formatResponseWithDistance(cmd_result_page);
				
		
		return EgoobeResponse.build(Response.Status.OK, null, result);

	}
	

	@GET
	@Produces("application/json")
	@Path("/{id}/comments")
	public Response getComments(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		DBCollection comments_coll = db.getCollection("comments");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the product was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList commentsList = (BasicDBList) product.get("comments");
		if(commentsList == null) { // verify if comments list wasn't initialized
			commentsList = new BasicDBList();
		}

		List<ObjectId> commentsListOID = new ArrayList<ObjectId>();
		for (Object comment : commentsList) { // Convert comments list to ObjectId list
			commentsListOID.add(new ObjectId(comment.toString()));
		}
		DBObject query_comments = new BasicDBObject("_id", new BasicDBObject("$in", commentsListOID));
		DBCursor cs = comments_coll.find(query_comments).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = CommentUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}/users")
	public Response getUsers(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		DBCollection users_coll = db.getCollection("users");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the product was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList usersList = (BasicDBList) product.get("users");
		if(usersList == null) { // verify if users list wasn't initialized
			usersList = new BasicDBList();
		}

		List<ObjectId> usersListOID = new ArrayList<ObjectId>();
		for (Object user : usersList) { // Convert users list to ObjectId list
			usersListOID.add(new ObjectId(user.toString()));
		}
		DBObject query_users = new BasicDBObject("_id", new BasicDBObject("$in", usersListOID));
		DBCursor cs = users_coll.find(query_users).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = UserUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
}
