package com.egoobe.rs.resources;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.bson.types.ObjectId;

import com.egoobe.mongo.Connector;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Path("/categories")
public class CategoriesResource {

	@GET
	@Produces("application/json")
	public String getCategory(
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection category_coll = db.getCollection("categories");
		DBCursor cs = category_coll.find().skip((page-1)*result_limit).limit(result_limit);
		List<DBObject> result = cs.toArray();
				
		return result.toString();
	}
	
	@GET
	@Produces("application/json")
	@Path("/{id}")
	public String getCategory(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection category_coll = db.getCollection("categories");
		DBObject query_id = new BasicDBObject("_id", new ObjectId(id));
		DBObject venue = category_coll.findOne(query_id);
		
		return venue.toString();
	}	
}
