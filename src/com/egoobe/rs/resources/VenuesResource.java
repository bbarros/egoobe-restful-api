package com.egoobe.rs.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;

import com.egoobe.mongo.Connector;
import com.egoobe.rs.core.EgoobeConsistency;
import com.egoobe.rs.core.EgoobeResponse;
import com.egoobe.rs.core.exceptions.BadParameterException;
import com.egoobe.rs.core.exceptions.ParameterNotFoundException;
import com.egoobe.utils.KeywordUtils;
import com.egoobe.utils.LatLngUtils;
import com.egoobe.utils.resource.CommentUtils;
import com.egoobe.utils.resource.ProductUtils;
import com.egoobe.utils.resource.UserUtils;
import com.egoobe.utils.resource.VenueUtils;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

@Path("/venues")
public class VenuesResource {

	@GET
	@Produces("application/json")
	public Response getVenues(
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBCursor cs = venues_coll.find().skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		List<DBObject> result = VenueUtils.formatResponse(csList);
				
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/{id}")
	public Response getVenue(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject query_id = new BasicDBObject("_id", new ObjectId(id));
		DBObject venue = venues_coll.findOne(query_id);
		
		BasicDBList csList = new BasicDBList();
		csList.add(venue);
		List<DBObject> result = VenueUtils.formatResponse(csList);
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	

	@GET
	@Produces("application/json")
	@Path("/search")
	public Response searchName(
			@DefaultValue("") @QueryParam("query") String query,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		BasicDBObject query_search = new BasicDBObject();
		
		// Handling query parameter
		if(query.compareTo("") != 0) {
			query_search.put("_keywords", new BasicDBObject("$in", KeywordUtils.getKeywords(query)));
		}
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		// Creating aggregation command
		BasicDBObject cmd = new BasicDBObject("aggregate", "venues");
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();
		
		BasicDBObject pipeline_match = new BasicDBObject("$match", query_search);
		pipeline.add(pipeline_match); // add match location and keywords
				
		if(query.compareTo("") != 0) { // sort by keywords relevance
			BasicDBObject pipeline_unwind = new BasicDBObject("$unwind", "$_keywords");
			pipeline.add(pipeline_unwind); // add unwind over _keywords fields

			BasicDBObject pipeline_match_query = new BasicDBObject("$match", new BasicDBObject("_keywords",new BasicDBObject("$in", KeywordUtils.getKeywords(query))));
			pipeline.add(pipeline_match_query); // add match over unwinded keywords
			
			BasicDBObject pipeline_group_fields = new BasicDBObject();
			pipeline_group_fields.put("_id", "$_id");
			pipeline_group_fields.put("name", new BasicDBObject("$first", "$name"));
			pipeline_group_fields.put("location", new BasicDBObject("$first", "$location"));
			pipeline_group_fields.put("relevance", new BasicDBObject("$sum", 1));
			BasicDBObject pipeline_group = new BasicDBObject("$group", pipeline_group_fields);
			pipeline.add(pipeline_group); // add group
			
			BasicDBObject pipeline_sort = new BasicDBObject("$sort", new BasicDBObject("relevance", -1));
			pipeline.add(pipeline_sort); // add sort			
		}
		
		BasicDBObject pipeline_skip = new BasicDBObject("$skip", (page-1)*result_limit);
		pipeline.add(pipeline_skip); // add skip
		
		BasicDBObject pipeline_limit = new BasicDBObject("$limit", result_limit);
		pipeline.add(pipeline_limit); // add limit
		
		cmd.put("pipeline", pipeline);
		CommandResult cmd_result = db.command(cmd);
		
		List<DBObject> result = VenueUtils.formatResponse((BasicDBList)cmd_result.get("result"));
				
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/searchnear")
	public Response searchNear(
			@DefaultValue("") @QueryParam("ll") String latlng,
			@DefaultValue("5000") @QueryParam("radius") String radius,
			@DefaultValue("") @QueryParam("query") String query,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		if(latlng.compareTo("") == 0) { // Check empty parameters
			throw new ParameterNotFoundException();
		}

		List<Double> latlnglist = new ArrayList<Double>();
		try { // check latlng parameter
			String[] vlatlng = latlng.split(",");
			latlnglist.add(new Double(vlatlng[0])); // add latitude
			latlnglist.add(new Double(vlatlng[1])); // add longitude
			
		} catch (Exception e) {
			throw new BadParameterException();
		}
		/* =================================================================================*/
		
		BasicDBObject geonear = new BasicDBObject("geoNear", "venues");
		BasicDBObject geonear_query = new BasicDBObject();

		geonear.put("near", latlnglist); // adding $near operator to location query
		
		// Handling radius parameter
		if(radius.compareTo("") != 0) {
			Double max_radius = LatLngUtils.metersToDegrees(new Double(radius));
			geonear.put("maxDistance", max_radius); // adding $maxDistance operator to location query			
		}
		
		// Handling num parameter
		geonear.put("num", result_limit*page);
				
		// Handling query parameter
		if(query.compareTo("") != 0) {
			geonear_query.put("_keywords", new BasicDBObject("$in", KeywordUtils.getKeywords(query)));
		}
		
		// Adding query to GeoNear Command
		geonear.put("query", geonear_query);
		
		// Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		CommandResult cmd_result = db.command(geonear);
		
		// Selecting result page
		BasicDBList cmd_result_list = (BasicDBList)cmd_result.get("results");
		BasicDBList cmd_result_page = new BasicDBList();
		for(int i=(page-1)*result_limit; i < cmd_result_list.size(); i++) {
			cmd_result_page.add(cmd_result_list.get(i));
		}
		
		List<DBObject> result = VenueUtils.formatResponseWithDistance(cmd_result_page);
				
		
		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@PUT
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	public Response insertVenue(
			@DefaultValue("") @FormParam("userid") String userid,
			@DefaultValue("") @FormParam("name") String name,
			@DefaultValue("") @FormParam("ll") String ll,
			@DefaultValue("") @FormParam("address") String address,
			@DefaultValue("") @FormParam("city") String city,
			@DefaultValue("") @FormParam("state") String state,
			@DefaultValue("") @FormParam("country") String country) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");

		DBCollection users_coll = db.getCollection("users");		
		DBObject user = null;		

		/*
		 *  Parameters Validation Routine ==================================================
		 */
		if(userid.compareTo("") == 0) {
			throw new ParameterNotFoundException();
		}
		
		try { // verify consistency of ObjectId for user id
			user = users_coll.findOne(new BasicDBObject("_id", new ObjectId(userid)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(user == null) {
			throw new ParameterNotFoundException();			
		}
				
		if(name.compareTo("") == 0 || ll.compareTo("") == 0) { // Check empty parameters
			throw new ParameterNotFoundException();
		}
		
		List<Double> llList = new ArrayList<Double>();
		try { // check ll parameter
			String[] vll = ll.split(",");
			llList.add(new Double(vll[0]));
			llList.add(new Double(vll[1]));
			
		} catch (Exception e) {
			throw new BadParameterException();
		}
		/* =================================================================================*/
		
		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBObject venue = new BasicDBObject();
		venue.put("name", name);
		BasicDBObject venue_loc = new BasicDBObject();
		venue_loc.put("address", address);
		venue_loc.put("ll", llList);
		venue_loc.put("city", city);
		venue_loc.put("state", state);
		venue_loc.put("country", country);
		venue.put("location", venue_loc);
		venue.put("createdby", userid);
		venue.put("_keywords", VenueUtils.generateKeywords(venue));
		
		DBCollection venues_coll = db.getCollection("venues");
		venues_coll.save(venue); // persist venue
		DBObject venueObj = venues_coll.findOne(venue); // retrieve venue
		
		BasicDBList csList = new BasicDBList();
		csList.add(venueObj);
		result = VenueUtils.formatResponse(csList);			

		return EgoobeResponse.build(Response.Status.CREATED, null, result);
	}
	

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}")
	public Response updateVenue(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("name") String name,
			@DefaultValue("") @FormParam("ll") String ll,
			@DefaultValue("") @FormParam("address") String address,
			@DefaultValue("") @FormParam("city") String city,
			@DefaultValue("") @FormParam("state") String state,
			@DefaultValue("") @FormParam("country") String country) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException(id);
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException(id);
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		if(name.compareTo("") != 0) {
			venue.put("name", name);			
		}
		
		BasicDBObject venue_loc = (BasicDBObject) venue.get("location");
		if(address.compareTo("") != 0) {
			venue_loc.put("address", address);			
		}

		
		if(ll.compareTo("") != 0) {
			List<Double> llList = new ArrayList<Double>();
			try { // check ll parameter
				String[] vll = ll.split(",");
				llList.add(new Double(vll[0]));
				llList.add(new Double(vll[1]));
				
			} catch (Exception e) {
				throw new BadParameterException();
			}
			venue_loc.put("ll", llList);
		}
		
		if(city.compareTo("") != 0) {
			venue_loc.put("city", city);
		}

		if(state.compareTo("") != 0) {
			venue_loc.put("state", state);			
		}

		if(country.compareTo("") != 0) {
			venue_loc.put("country", country);			
		}
		
		venue.put("location", venue_loc);
		venue.put("_keywords", VenueUtils.generateKeywords(venue));
		
		venues_coll.save(venue); // persist venue
		DBObject venueObj = venues_coll.findOne(venue); // retrieve venue
		
		BasicDBList csList = new BasicDBList();
		csList.add(venueObj);
		result = VenueUtils.formatResponse(csList);			

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@DELETE
	@Produces("application/json")
	@Path("/{id}")
	public Response deleteVenue(@PathParam("id") String id) {
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		// Removing entity relations
		EgoobeConsistency.deleteRelations(EgoobeConsistency.VENUE_ENTITY, id);
		
		DBCollection venues_coll = db.getCollection("venues");

		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId
			venues_coll.remove(new BasicDBObject("_id", new ObjectId(id)));
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException(id);
		}
		/* =================================================================================*/

		return EgoobeResponse.build(Response.Status.NO_CONTENT, null, null);
	}
	
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/addproduct")
	public Response addProduct(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("productId") String productId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(productId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the product was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update Venue document
		BasicDBList productList = (BasicDBList) venue.get("products");
		if(productList == null) { // verify if product list wasn't initialized
			productList = new BasicDBList();
		}
		
		if(!productList.contains(productId)) { // verify replication
			productList.add(productId);
			venue.put("products", productList);
			venues_coll.save(venue);
		}
				
		// Update Product document
		BasicDBList venuesList = (BasicDBList) product.get("venues");
		if(venuesList == null) { // verify if venue list wasn't initialized
			venuesList = new BasicDBList();
		}
		
		if(!venuesList.contains(id)) { // verify replication
			venuesList.add(id);
			product.put("venues", venuesList);
			products_coll.save(product);
		}
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}


	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/delproduct")
	public Response deleteProduct(
			@PathParam("id") String id,
			@DefaultValue("") @FormParam("productId") String productId) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}

		try { // verify consistency of ObjectId for product id
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(productId)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(product == null) { // check if the product was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update Venue document
		BasicDBList productList = (BasicDBList) venue.get("products");
		if(productList != null) { // verify if product list wasn't initialized
			if(productList.contains(productId)) { // verify replication
				productList.remove(productId);
				venue.put("products", productList);
				venues_coll.save(venue);
			}
		}
				
		// Update Product document
		BasicDBList venuesList = (BasicDBList) product.get("venues");
		if(venuesList != null) { // verify if venue list wasn't initialized
			if(venuesList.contains(id)) { // verify replication
				venuesList.remove(id);
				product.put("venues", venuesList);
				products_coll.save(product);
			}
		}
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}


	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	@Path("/{id}/cleanproducts")
	public Response cleanProducts(@PathParam("id") String id) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		DBObject product = null;
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		// Update Product document
		BasicDBList productList = (BasicDBList) venue.get("products");
		if(productList == null) { // verify if product list wasn't initialized
			productList = new BasicDBList();
		}
		
		for (Object productId : productList) {
			String pid = productId.toString();
			product = products_coll.findOne(new BasicDBObject("_id", new ObjectId(pid)));
			if(product != null) {
				BasicDBList venuesList = (BasicDBList) product.get("venues");
				if(venuesList != null) { // verify if venue list wasn't initialized
					if(venuesList.contains(id)) { // verify replication
						venuesList.remove(id);
						product.put("venues", venuesList);
						products_coll.save(product);
					}
				}
			}
		}
		
		// Update Venue document		
		venue.removeField("products");
		venues_coll.save(venue);
		
		return EgoobeResponse.build(Response.Status.OK, null, null);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}/products")
	public Response getProducts(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection products_coll = db.getCollection("products");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList productList = (BasicDBList) venue.get("products");
		if(productList == null) { // verify if product list wasn't initialized
			productList = new BasicDBList();
		}

		List<ObjectId> productListOID = new ArrayList<ObjectId>();
		for (Object product : productList) { // Convert product list to ObjectId list
			productListOID.add(new ObjectId(product.toString()));
		}
		DBObject query_products = new BasicDBObject("_id", new BasicDBObject("$in", productListOID));
		DBCursor cs = products_coll.find(query_products).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = ProductUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}
	
	
	@GET
	@Produces("application/json")
	@Path("/{id}/comments")
	public Response getComments(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection comments_coll = db.getCollection("comments");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList commentsList = (BasicDBList) venue.get("comments");
		if(commentsList == null) { // verify if comments list wasn't initialized
			commentsList = new BasicDBList();
		}

		List<ObjectId> commentsListOID = new ArrayList<ObjectId>();
		for (Object comment : commentsList) { // Convert comments list to ObjectId list
			commentsListOID.add(new ObjectId(comment.toString()));
		}
		DBObject query_venues = new BasicDBObject("_id", new BasicDBObject("$in", commentsListOID));
		DBCursor cs = comments_coll.find(query_venues).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = CommentUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

	
	@GET
	@Produces("application/json")
	@Path("/{id}/users")
	public Response getUsers(@PathParam("id") String id,
			@DefaultValue("1") @QueryParam("page") int page,
			@DefaultValue("10") @QueryParam("limit") int result_limit) {
		
		//Connecting to MongoDB using connector
		Connector mongoconn = Connector.getInstance();
		DB db = mongoconn.getDb("egoobe");
		
		DBCollection venues_coll = db.getCollection("venues");
		DBObject venue = null;
		DBCollection users_coll = db.getCollection("users");
		
		/*
		 *  Parameters Validation Routine ==================================================
		 */
		try { // verify consistency of ObjectId for venue id
			venue = venues_coll.findOne(new BasicDBObject("_id", new ObjectId(id)));			
		} catch (Exception e) {
			e.printStackTrace();
			throw new BadParameterException();
		}
		
		if(venue == null) { // check if the venue was found
			throw new BadParameterException();
		}
		/* =================================================================================*/

		List<DBObject> result = new ArrayList<DBObject>();
		
		BasicDBList usersList = (BasicDBList) venue.get("users");
		if(usersList == null) { // verify if users list wasn't initialized
			usersList = new BasicDBList();
		}

		List<ObjectId> usersListOID = new ArrayList<ObjectId>();
		for (Object user : usersList) { // Convert users list to ObjectId list
			usersListOID.add(new ObjectId(user.toString()));
		}
		DBObject query_users = new BasicDBObject("_id", new BasicDBObject("$in", usersListOID));
		DBCursor cs = users_coll.find(query_users).skip((page-1)*result_limit).limit(result_limit);

		BasicDBList csList = new BasicDBList();
		csList.addAll(cs.toArray());
		result = UserUtils.formatResponse(csList);

		return EgoobeResponse.build(Response.Status.OK, null, result);
	}

}
