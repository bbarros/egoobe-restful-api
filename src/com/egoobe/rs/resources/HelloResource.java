package com.egoobe.rs.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Path("/hello")
public class HelloResource {
	
	@GET
	@Produces("application/json")
	public String getMessage() {
		// Building Root Node
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("message", "Hello World!");		
		// Building Meta Node
		ObjectNode metaNode = mapper.createObjectNode();
		metaNode.put("code", 200);
		metaNode.put("message", "Ok!");
		rootNode.put("meta", metaNode);
		
		return rootNode.toString();
	}
}
