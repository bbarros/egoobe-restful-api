package com.egoobe.mongo;

import java.net.UnknownHostException;

import com.egoobe.rs.common.PropertiesReader;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;

public class Connector {

	private static Connector instance;
	private static DB egoobeDB;
	
	private Connector() {
		PropertiesReader pr = PropertiesReader.getInstance();
		
		try {
			String islocal = pr.getProperty("mongo.islocal");
			if(Boolean.valueOf(islocal)) {
				egoobeDB = new MongoClient("localhost" , 27017).getDB("egoobe");				
			} else {
				MongoClientURI uri = new MongoClientURI(pr.getProperty("mongo.remoteuri"));
				egoobeDB = new MongoClient(uri).getDB(pr.getProperty("mongo.remotedbname"));
				egoobeDB.authenticate(uri.getUsername(), uri.getPassword());				
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized Connector getInstance() {
		if (instance == null) {
			instance = new Connector();
		}
		
		return instance;
	}
	
	public DB getDb(String dbname) {
		return egoobeDB;
	}
}
